
%{

typedef struct
{
 char thestr[2000];
 char strlit[2000];
 int ival;
 int ttype;
}tstruct ;

#define YYSTYPE  tstruct

#include "w.tab.h"
#define YY_USER_ACTION yylloc.first_line = yylloc.last_line = yylineno;
%}

%option yylineno


DIGIT     [0-9]
UPPER     [A-Z]
LOWER     [a-z]
LETTER    [A-Za-z]

%%
START	      { return tstart;}
FINISH	      { return tfinish;}
CREATE        { return tcreate;}
INT           { return tint;}
STR           { return tstring;}
PRINTHORIZ    { return thoriz;}
PRINTVERT     { return tvert;}
"="           { return tassign;}

\"([^"]*)\"   { strcpy( yylval.strlit, yytext); return tstrlit; }
{LETTER}+     { strcpy( yylval.thestr, yytext); return tid; }
{DIGIT}+ { yylval.ival = atoi(yytext); return tnum; }


@@.*\n   {} /* comments  */   


[ \t]     /* ignore whitespace */

\n       {}

<<EOF>>   yyterminate();  /* signal end of dialogue */

.         return yytext[0];

%%


void yyerror(char *s)  /* Called by yyparse on error */
{
  printf ("\terror: %s\n", s);
  printf ("ERROR: %s at line %d\n", s, yylineno);
}

