
%{

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


typedef struct 
{
 char thestr[2000];
 char strlit[2000];
 int ival;
 int ttype;
}tstruct ; 

#define YYSTYPE  tstruct 



int yylex();
void yyerror( char *s );


#include "symtab.c"


%}

%token tstart
%token tfinish
%token tcreate
%token tint    
%token thoriz
%token tvert
%token tassign  
%token tstrlit  
%token tid  
%token tnum  
%token tstring

%define locations

%%

prog : tstart DL SL tfinish
                          {
			    printf("#include <stdio.h>\n");
			    printf("#include <stdlib.h>\n");
			    printf("#include <string.h>\n");
			    printf("\n");
		            printf("int main()\n");
			    printf("{\n");
			    printf("%s", $2.thestr);
			    printf("%s", $3.thestr);
			    printf("return 0;\n");
			    printf("}\n");
			  }
 ;

DL :  DL D   {sprintf($$.thestr, "%s%s", $1.thestr, $2.thestr);}
  | D        {sprintf($$.thestr, "%s", $1.thestr);	       }
  ;


D : tcreate type tid ';' { addtab($3.thestr);
			   addtype($3.thestr, $2.ttype);
			   $$.ttype = $2.ttype;
                           if($2.ttype == 10) 
			     {sprintf($$.thestr,"int %s;\n",$3.thestr);}
			   if($2.ttype == 20)
			     {sprintf($$.thestr,"char * %s;\n",$3.thestr);}
			 } 

type: tint {$$.ttype = 10;} | tstring {$$.ttype = 20;};

SL :  SL S   {sprintf($$.thestr, "%s%s", $1.thestr, $2.thestr);}
  | S        {sprintf($$.thestr, "%s", $1.thestr);}
  ;

S :  thoriz tid ';'      
              {
                if ( intab($2.thestr) )
                   {
		     if(gettype($2.thestr) == 20)
			sprintf($$.thestr,"printf(\"%cs\\n\",%s);\n",'%',$2.thestr);
		     else if(gettype($2.thestr) == 10)
			sprintf($$.thestr,"printf(\"%cd\\n\",%s);\n",'%',$2.thestr);
		   }
                else
                   printf("UNDECLARED:: %s,(line %d) \n", $2.thestr, yyloc.first_line);
              }
  | tvert tid ';'
		{
			if( intab($2.thestr) )
			{
				
				{	if(gettype($2.thestr) == 20)
					{
						sprintf($$.thestr, "for(int ITERATOR = 0; ITERATOR < strlen(%s); ITERATOR++)\n", $3.thestr);
						char temp [2000];
						sprintf(temp, "{\nprintf(\"%cc\\n\",%s[ITERATOR]);\n}\n",'%',$2.thestr);
						strcat($$.thestr, temp);
						strcat($$.thestr, "\n");
					}
					else if (gettype($2.thestr) == 10)
					{
						printf("Error: only strings may be printed vertically\n");
					}
				}
			}
		else
		   printf("UNDECLARED:: %s,(line %d) \n", $2.thestr, yyloc.first_line);
		}
  |  tid tassign expr ';'  
              {
               /* printf("assign\n");*/
                if ( intab($1.thestr) )
                   sprintf($$.thestr,"%s = %s;\n", 
                           $1.thestr, $3.thestr);
                else
                   printf("UNDECLARED:: %s \n", $1.thestr);

                $1.ttype = gettype($1.thestr);
                if ($1.ttype > 0 )
                {
                  if ($1.ttype == $3.ttype)
                        ;
                  else
                     {
                      printf("Incompatible ASSIGN types %d %d\n", $1.ttype, $3.ttype);
                     }
                }
                else 
                    yyerror("Type Error :::");
	      }
  | tid tassign tstrlit ';'
	     {
		if ( intab($1.thestr) )
		   sprintf($$.thestr, "%s = %s;\n", $1.thestr, $3.strlit);
		else
		   printf("UNDECLARED:: %s \n", $1.thestr);
	     }
         
  | error ';'    {printf("error in statement\n");}
  ;

expr : expr '+' term 
             { 
               if ($1.ttype == 10 && $3.ttype == 10)  $$.ttype = 10;
               else if ($1.ttype == 20 && $3.ttype == 20)  $$.ttype = 20;
               else $$.ttype = -1;
	     }
  |  expr '-' term
	     {
	       if ($1.ttype == 10 && $3.ttype == 10)  $$.ttype = 10;
	       else if ($1.ttype == 20 && $3.ttype == 20)  $$.ttype = 20;
	       else $$.ttype = -1;
	     }
  |  term      { 
		strcpy($$.thestr, $1.thestr);
		$$.ival = $1.ival;
		$$.ttype = $1.ttype; }
  ;

term : term '*' factor
             { 
               if ($1.ttype == 10 && $3.ttype == 10)  $$.ttype = 10; 
               else if ($1.ttype == 20 && $3.ttype == 20)  $$.ttype = 20;
               else $$.ttype = -1; 
             }
  | term '/' factor
	  {
	       if ($1.ttype == 10 && $3.ttype == 10)  $$.ttype = 10;
	       else if ($1.ttype == 20 && $3.ttype == 20)  $$.ttype = 20;
	       else $$.ttype = -1;
	  }
  | factor   { 
		strcpy($$.thestr, $1.thestr);
		$$.ival = $1.ival;
		$$.ttype = $1.ttype; }
  ;

factor : tid  
              {
                if ( intab($1.thestr) )
                   printf("%s is declared\n", $1.thestr);
                else
                   printf("UNDECLARED:: %s \n", $1.thestr);
                $$.ttype = gettype($1.thestr);
                if ($$.ttype > 0 )
                      ;
                else 
                    yyerror("Type Error :::");
              }
 | tnum     {$$.ttype = 10;
	     sprintf($$.thestr, "%s", $1.thestr);}
 | tstring  {$$.ttype = 20;
	     sprintf($$.thestr, "%s", $1.thestr);}
 ;

%%


int main()
{
  yyparse ();
}
